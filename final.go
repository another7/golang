package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	ones_Romes := [11]string{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"}
	ones_Arab := [11]string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}
	operands := [4]string{"+", "-", "*", "/"}

	// сканирование и занесение данных в массив
	in := bufio.NewScanner(os.Stdin)
	in.Scan()
	nums := strings.Split(in.Text(), " ")

	// проверка массива на правильность ввода
	if len(nums) > 3 {
		fmt.Println("ошибка формат операции не удовлетворяет заданию два операнда и один оператор")
	} else if len(nums) < 3 {
		fmt.Println("ошибка строка не является математической операцией")
	}

	// ошибка ввода оператора
	if operands[0] != nums[1] &&
		operands[1] != nums[1] &&
		operands[2] != nums[1] &&
		operands[3] != nums[1] {
		fmt.Println("ошибка формат операции не удовлетворяет заданию два операнда и один оператор")
	}

	/* сравнение чисел на соответсвие ввода.
	Римские с Римским, Арабские с Арабскими.
	Перевод римских чисел в арабскую систему исчеления для проведения операции
	*/
	num1 := nums[0]
	num2 := nums[1]
	num3 := nums[2]

	count_R := 0
	count_A := 0

	for i := range ones_Romes {
		if ones_Romes[i] == num1 {
			count_R += 1
			for j := range ones_Romes {
				if ones_Romes[j] == num3 {
					count_R += 1
					foo_calc_Rom(ones_Arab[i], num2, ones_Arab[j])
				} else if ones_Arab[j] == num3 {
					fmt.Println("Вывод ошибки, так как используются одновременно разные системы счисления.")
				}
			}
		}
	}

	for i := range ones_Arab {
		if ones_Arab[i] == num1 {
			count_A += 1
			for j := range ones_Arab {
				if ones_Arab[j] == num3 {
					count_A += 1
					foo_calc_Arab(num1, num2, num3)
				} else if ones_Romes[j] == num3 {
					fmt.Println("Вывод ошибки, так как используются одновременно разные системы счисления.")
				}
			}
		}
	}

	if count_R == 0 || count_R == 1 || count_A == 0 || count_A == 1 {
		fmt.Println("ошибка ввода,числа должны быть только от 1 до 10 включительно или от I до X включительно")
	}

}

func foo_calc_Arab(a string, oper string, b string) {

	num1_int, err := strconv.Atoi(a)
	if err != nil {
		log.Fatal(err)
	}
	num2_int, err := strconv.Atoi(b)
	if err != nil {
		log.Fatal(err)
	}

	if oper == "+" {
		fmt.Println(num1_int + num2_int)
	} else if oper == "-" {
		fmt.Println(num1_int - num2_int)
	} else if oper == "*" {
		fmt.Println(num1_int * num2_int)
	} else if oper == "/" {
		fmt.Println(num1_int / num2_int)
	}
}

func foo_calc_Rom(a string, oper string, b string) {

	num1_int, err := strconv.Atoi(a)
	if err != nil {
		log.Fatal(err)
	}
	num2_int, err := strconv.Atoi(b)
	if err != nil {
		log.Fatal(err)
	}

	if oper == "+" {
		checkio(num1_int + num2_int)
	} else if oper == "-" {
		if num1_int-num2_int <= 0 {
			fmt.Println("Вывод ошибки, так как в римской системе нет отрицательных чисел.")
		} else {
			fmt.Println(num1_int - num2_int)
		}
	} else if oper == "*" {
		checkio(num1_int * num2_int)
	} else if oper == "/" {
		checkio(num1_int / num2_int)
	}
}

func checkio(data int) {

	ones_Romes := [11]string{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"}
	tens := [11]string{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC", "C"}

	te := tens[data/10]
	o := ones_Romes[data%10]

	fmt.Println(te + o)
}
